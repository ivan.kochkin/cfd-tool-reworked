from Script.DWH.Access import get_dwh_password
from Script.DWH.Queries import *
import pandas as pd
import psycopg2
import sys


def call_query(sql, currency, params=None):
    """ Takes a SQL query,
    --> opens a connection
    --> queries the data, puts them into a pandas dataframe
    --> closes the connection
    returns the dataframe """
    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"
    dbname = "production"
    user = f"{currency}_{dbname}"
    password = get_dwh_password()
    port = "5439"

    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"

    with psycopg2.connect(connect_string) as conn:
        out_df = pd.read_sql_query(sql=sql, con=conn, params=params)

    return out_df


def dwh_import(**kwargs):
    no_of_scenario_in_name = call_query(sql=scenarios_grom_name(**kwargs), currency=kwargs['currency'])
    if int(no_of_scenario_in_name['count']) != 1 and "scenario_hash" not in kwargs:
        sys.exit('ABORT: please input DWH scenario hash in the Value column of the control sheet')
    for query in (price_query, plant_data_query, CanProduction_query, Capacity_query):
        yield call_query(sql=query(**kwargs), currency=kwargs['currency'])


def dwh_process(run, data):

    dwh_price, plant_data, can_production_data, capacity_data = data

    plant_data = plant_data.set_index("plant_id")
    plant_data = plant_data[plant_data.technology.isin(run.tech)]
    if run.subregions_method == 'white_list':
        plant_data = plant_data[plant_data["subregion"].isin(run.subregions)]
    else:
        plant_data = plant_data[-plant_data["subregion"].isin(run.subregions)]

    if len(plant_data) == 0:
        sys.exit(
            'ABORT: ' + run.name
            + ', no plants with technologies (' + ', '.join(sorted(run.tech)) + ')'
            + ' in regions (' + ','.join(run.subregions) + ')'
        )

    can_production_data = pd.pivot_table(
        can_production_data,
        values='canproductioninmw',
        index='timestamp',
        columns='plant_id'
    )
    capacity_data["capacity"] = capacity_data["capacity"].multiply(1000)
    capacity_data = pd.pivot_table(
        capacity_data,
        values='capacity',
        index='date',
        columns='plant_id'
    ).resample('H').ffill()
    plant_ids = capacity_data.columns

    can_production_data = can_production_data.merge(
        capacity_data,
        how="left",
        left_index=True,
        right_index=True,
        suffixes=('_prod', '_cap')
    )
    for plant_id in plant_ids:
        can_production_data[plant_id] = can_production_data[f'{plant_id}_prod']/can_production_data[f'{plant_id}_cap']
    can_production_data = can_production_data[plant_ids]
    lf = can_production_data.mean(axis=1).rename('LF')

    dwh_price = dwh_price.set_index(["timestamp"])
    processed_data = pd.merge(dwh_price, lf, left_index=True, right_index=True)
    processed_data = processed_data.reset_index()
    processed_data = processed_data.rename(columns={"timestamp": "Timestamp", "price": "Price"})
    return processed_data
