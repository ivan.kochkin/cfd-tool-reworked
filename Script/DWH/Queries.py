
def price_query(**kwargs):
    return """ select time.Date as Timestamp, HalfHourlyRegion.WholesalePrice as Price from HalfHourlyRegion 
    join Regions
    on
    Regions.id = HalfHourlyRegion.RegionID
    join Scenarios
    on
    Scenarios.id = HalfHourlyRegion.ScenarioID
    join time
    on
    time.id = HalfHourlyRegion.TimeId
    where 
    """ + (
        ("Scenarios.hash = '" + kwargs.get("scenario_hash", "") + "'")
        if "scenario_hash" in kwargs
        else ("Scenarios.scenario = '" + kwargs["scenario_name"] + "'")
    ) + f"""
    and Regions.Region = '{kwargs["region"]}'
    and time.date >= '{kwargs["start_date"]}'
    and time.date < '{kwargs["end_date"]}'
    """


def plant_data_query(**kwargs):
    return """ select Plants.id as Plant_id, Regions.Region as region, PlantSettings.subregion as subregion, Technologies.Technology as technology from PlantSettings 
    join Scenarios 
    on
    Scenarios.id = PlantSettings.ScenarioID
    join Plants
    on Plants.id = PlantSettings.PlantID
    join Technologies
    on Technologies.id = plants.TechnologyID
    join Regions
    on
    Regions.id = plants.RegionID
    where 
    """ + (
        ("Scenarios.hash = '" + kwargs.get("scenario_hash", "") + "'")
        if "scenario_hash" in kwargs
        else ("Scenarios.scenario = '" + kwargs["scenario_name"] + "'")
    ) + f"""
    and Regions.Region = '{kwargs["region"]}'
    """


def CanProduction_query(**kwargs):
    return """ select HalfHourlyPlant.PlantID as Plant_id, time.Date as timestamp, HalfHourlyPlant.CanProductionInMw as CanProductionInMw from HalfHourlyPlant 
    join Scenarios 
    on
    Scenarios.id = HalfHourlyPlant.ScenarioID
    join time
    on
    time.id = HalfHourlyplant.TimeId
    join Plants
    on Plants.id = HalfHourlyplant.PlantID
    join Technologies
    on Technologies.id = plants.TechnologyID
    join Regions
    on
    Regions.id = plants.RegionID
    where 
    """ + (
        ("Scenarios.hash = '" + kwargs.get("scenario_hash", "") + "'")
        if "scenario_hash" in kwargs
        else ("Scenarios.scenario = '" + kwargs["scenario_name"] + "'")
    ) + f"""
    and Regions.Region = '{kwargs["region"]}'
    and time.date >= '{kwargs["start_date"]}'
    and time.date < '{kwargs["end_date"]}'
    and Technologies.Technology in ({','.join(sorted(f"'{x}'" for x in kwargs["technologies"]))})
    """


def Capacity_query(**kwargs):
    return """ select PlantID as plant_id, time.Date,Capacity from YearlyPlantOperations
    join Plants
    on Plants.id = YearlyPlantOperations.PlantID
    join Technologies
    on Technologies.id = plants.TechnologyID
    join Scenarios
    on
    Scenarios.id = YearlyPlantOperations.ScenarioID
    join time
    on
    time.id = YearlyPlantOperations.TimeId
    join Regions
    on
    Regions.id = plants.RegionID
    where
    """ + (
        ("Scenarios.hash = '" + kwargs.get("scenario_hash", "") + "'")
        if "scenario_hash" in kwargs
        else ("Scenarios.scenario = '" + kwargs["scenario_name"] + "'")
    ) + f"""
    and Regions.Region = '{kwargs["region"]}'
    and time.year >= {kwargs["start_date"].year}
    and time.year <= {kwargs["end_date"].year+1}
    and Technologies.Technology in ({','.join(sorted(f"'{x}'" for x in kwargs["technologies"]))})
    """


def scenarios_grom_name(**kwargs):
    return """ select count(*) from Scenarios where scenario = '""" + kwargs["scenario_name"] + """'
    """
