"""
Taken from https://gitlab.com/aermod/software-dev/aurora-dragonfly-model-run/-/blob/08c110a529a2c30adda2103b0ded1387bc64c96c/aurora/core/token.py

Easier than adding another requirement, but should be identical
"""

from os import environ
from pathlib import Path
from getpass import getpass, getuser

# attempt to import keyring
try:
    import keyring

    has_keyring = True
except ImportError:
    has_keyring = False

# attempt to import boto3
try:
    import boto3

    has_boto3 = True
except ImportError:
    has_boto3 = False

from logging import getLogger

logger = getLogger(__name__)

# some constants
AURORA_API_KEY_ENV = "AURORA_API_KEY"
AURORA_API_KEY_FILE = ".aurora-api-key"


def get_token(
    user_name: str = getuser(),
    service: str = "aer-api",
    enable_keyring: bool = True,
    enable_ssm: bool = True,
    environment_variable: str = AURORA_API_KEY_ENV,
    key_file: str = AURORA_API_KEY_FILE,  # relative to ${HOME}
    ssm_path: str = None,
    interactive: bool = True,
):
    # try by using the environment variables
    # if it exists and isn't empty...
    if environment_variable in environ and environ.get(environment_variable):
        logger.debug("token found in the environment variable `%s`.", environment_variable)
        return environ.get(environment_variable)

    # try checking user's API KEY file
    api_file = Path.joinpath(Path.home(), key_file)
    # if it exists...
    if api_file.is_file():
        # TODO check that the file is only readable by the user

        token = api_file.read_text().strip()
        # ... and isn't empty
        if token:
            logger.debug("token found in the file `%s`.", key_file)
            return token

    # try using the system keychain
    if enable_keyring and has_keyring:
        token = keyring.get_password(service, user_name)
        if token:
            logger.debug(
                "token found in the system's `keyring` (`%s`@`%s`).",
                user_name,
                service,
            )
            return token

    # try using aws ssm
    if enable_ssm and has_boto3:
        # create the path if it hasn't been given to us
        ssm_path = ssm_path or f"/{service}/{user_name}/token"
        try:
            client = boto3.client("ssm")
            response = client.get_parameter(Name=ssm_path, WithDecryption=True)
            if response["Parameter"]["Value"]:
                logger.debug("token found in ssm path `%s`", ssm_path)
                return response["Parameter"]["Value"]
        except Exception:
            pass

    # last try, user input
    if interactive:
        token = getpass("enter token: ")
        if token:
            return token

        raise RuntimeError("i won't allow an empty password.")

    raise RuntimeError("i can't find a token anywhere, and you know i tried!")


def get_dwh_password():
    return get_token(
        user_name="production",
        service="DWH",
        enable_keyring=True,
        enable_ssm=False,
        environment_variable="PGPASSWORD",
        key_file=".aurora-dwh-key",
        interactive=False,
    )
