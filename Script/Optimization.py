from Script.DWH.DWH_processing import dwh_process
from Script.Tools import scaling_by_method
import pandas as pd
import warnings


def create_daily(control, run, data):
    data["Timestamp"] = pd.to_datetime(data["Timestamp"])
    data = data[data["Timestamp"].dt.date >= run.start_date]
    data = data[data["Timestamp"].dt.date < run.end_date]

    #  Rescale LFs if wanted
    if run.rescale_LF:
        warnings.warn(
            f"Rescaling Load Factors series to match avg yearly load factors in {control.local_inputs['target_LF']}"
        )
        if isinstance(run.target_LF, str):
            target_lf = pd.read_csv(control.local_inputs['target_LF'], index_col=0, header=0)
            target_lf = target_lf[run.target_LF].dropna()
        else:
            target_lf = pd.DataFrame(index=sorted(set(data['Timestamp'].year)))
            target_lf['LF'] = run.target_LF
            target_lf = target_lf['LF']
        for yr in target_lf.index:
            lf_series = data[data["Timestamp"].dt.year == yr]["LF"]
            target_avg_lf = target_lf.loc[yr]
            new_series = scaling_by_method[control.scaling_method](lf_series, target_avg_lf)
            data.loc[new_series.index, "LF"] = new_series

    #  Add production column, remove unwanted columns
    capacity = pd.DataFrame(data=run.capacity_to_MW, index=range(len(data.index)), columns=["Capacity"])
    data = pd.concat([data, capacity], axis=1)
    data["Production"] = data["LF"] * data["Capacity"]
    data = data.drop(["Capacity", "LF"], axis=1)
    data["Revenue"] = data["Price"] * data["Production"]

    segments = pd.DataFrame(
        data=int(1), index=range(len(data.index)), columns=["Segments"]
    )
    data = pd.concat([data, segments], axis=1)
    data["Date"] = data["Timestamp"].dt.floor("d")

    daily = data.groupby(["Date"]).sum()
    daily = daily.reset_index()

    daily['Price'] = daily['Price'] / daily['Segments']  # Correct price to make it the daily average from the sum
    daily['Capture'] = daily['Revenue'] / daily['Production']

    daily = daily.drop(["Segments"], axis=1)
    daily["CfD_bonus"] = run.strike_price - daily["Price"]
    return daily


def cfd_optimisation(data, control, run, period):
    """
    Function to optimise revenue of a CfD plant with a given production share by selecting which days to dedicate to CfD
    contract and which days to run as merchant, see documentation linked below for methodology
    https://auroraenergy.atlassian.net/wiki/spaces/AW/pages/3323199729/CfD+Reporting+Optimisation+Tool

    Inputs:  period -    dataframe, sorted by 'Price', with information on period of days (i.e. daily values) to
                         undergo optimisation, this should contain: daily production 'Production', revenue 'Revenue',
                         capture price 'Capture', average price 'Price', CfD bonus 'CfD_bonus'
             shares -    array of shares of production dedicated to CfD
             strike -    the CfD strike price

    Outputs: all desired output values of optimisation in form to be added to output dataframes
"""
    data = data.sort_values(by=['Price'])
    data.reset_index()

    generation = data['Production'].sum()
    capture_revenue = data['Revenue'].sum()
    period.update({
        "Share_Name": "place_holder",
        "Share": "place_holder",
        "Generation": generation,
        "Capture_Revenue": capture_revenue
    })
    #  Initialise outputs
    outputs_base = pd.DataFrame(
        index=[0],
        data=period
    )
    outputs_base["Capture_Price"] = outputs_base["Capture_Revenue"] / outputs_base["Generation"]

    outputs = None
    for share_name in control.shares.columns:
        share = control.shares.loc[period['Year'], share_name]
        outputs_base["Share_Name"] = share_name
        outputs_base["Share"] = share
        unopt_cfd_bonus_rev = (data['Production'] * data['CfD_bonus'] * share).sum()

        # Initialise important values as 0
        cfd_generation = 0.0
        cfd_revenue = 0.0
        cfd_capture_revenue = 0.0
        penalty_cost = 0.0

        for count, i in enumerate(data.index):
            if cfd_generation < 0.85 * share * generation:  # reported less than legally required 85% of generation share
                if data["CfD_bonus"][i] < - (run.strike_price / 2) and run.penalty:
                    # possibly optimal to take penalty payment, need to see if overall cost of reaching 85% of share is more than cost of penalty payment
                    total_penalty_cost = 0.5 * run.strike_price * (share * generation - cfd_generation)
                    cfd_generation_temp = cfd_generation
                    cfd_revenue_temp = cfd_revenue
                    cfd_capture_revenue_temp = cfd_capture_revenue
                    total_cfd_bonus_revenue = 0
                    # now calculate total cfd_bonus_revenue to reach 85% of generation share and avoid penalty
                    for j in data.index.to_list()[count:]:  # loop over remaining periods and add to temp CfD parameters
                        if cfd_generation_temp < 0.85 * share * generation:  # probably not needed but here to make sure no unneeded loops
                            if cfd_generation_temp + data["Production"][j] < 0.85 * share * generation:
                                # add whole period as when added the total is still under 85% of share
                                cfd_gen_add = data["Production"][j]
                                cfd_generation_temp += cfd_gen_add
                                cfd_revenue_temp += (data['Capture'][j] + data['CfD_bonus'][j]) * cfd_gen_add
                                cfd_capture_revenue_temp += data['Revenue'][j]
                                total_cfd_bonus_revenue += data['CfD_bonus'][j] * cfd_gen_add

                            else:  # only add part of period needed to reach 85% of share as no incentive to exceed 85%
                                cfd_gen_add = (0.85 * share * generation) - cfd_generation_temp
                                cfd_generation_temp += cfd_gen_add
                                cfd_revenue_temp += (data['Capture'][j] + data['CfD_bonus'][j]) * cfd_gen_add
                                cfd_capture_revenue_temp += data['Revenue'][j]
                                total_cfd_bonus_revenue += data['CfD_bonus'][j] * cfd_gen_add
                                break  # stop loop as now have reached 85% of generation share

                    if total_cfd_bonus_revenue > - total_penalty_cost:
                        # more profitable to reach 85% of generation share, take temp CfD parameters as actual
                        cfd_generation = cfd_generation_temp
                        cfd_revenue = cfd_revenue_temp
                        cfd_capture_revenue = cfd_capture_revenue_temp
                    else:
                        # more profitable to accept penalty payment, do not add to CfD reported generation and accept penalty
                        penalty_cost = total_penalty_cost

                    break  # stop initial loop as have fully optimised the period here

                else:  # no penalty payments or penalty not optimal
                    if cfd_generation + data["Production"][i] < 0.85 * share * generation:
                        # add whole period as when added the total is still under 85% of share
                        cfd_gen_add = data["Production"][i]
                        cfd_generation += cfd_gen_add
                        cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                        cfd_capture_revenue += data['Revenue'][i]

                    elif data["CfD_bonus"][i] > 0:  # getting a bonus from CfD so is optimal to report generation
                        if cfd_generation + data["Production"][i] < share * generation:
                            # add whole period as when added the total is still under 100% of share
                            cfd_gen_add = data["Production"][i]
                            cfd_generation += cfd_gen_add
                            cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                            cfd_capture_revenue += data['Revenue'][i]

                        else:  # only add part of period needed to reach 100% of share as cannot exceed 100% of share
                            cfd_gen_add = (share * generation) - cfd_generation
                            cfd_generation += cfd_gen_add
                            cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                            cfd_capture_revenue += (cfd_gen_add / data["Production"][i]) * data['Revenue'][i]
                            break  # stop initial loop as generation share fully reported in optimal way

                    else:  # not getting a bonus from CfD, only add part of period needed to reach 85% of share as no incentive to exceed 85%
                        cfd_gen_add = (0.85 * share * generation) - cfd_generation
                        cfd_generation += cfd_gen_add
                        cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                        cfd_capture_revenue += (cfd_gen_add / data["Production"][i]) * data['Revenue'][i]
                        break  # stop initial loop as have fully optimised the period here

            elif cfd_generation < share * generation and data["CfD_bonus"][i] > 0:
                # reported generation less than 100% of share and getting a bonus from CfD so is optimal to report generation
                if cfd_generation + data["Production"][i] < share * generation:
                    # add whole period as when added the total is still under 100% of share
                    cfd_gen_add = data["Production"][i]
                    cfd_generation += cfd_gen_add
                    cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                    cfd_capture_revenue += data['Revenue'][i]

                else:  # only add part of period needed to reach 100% of share as cannot exceed 100% of share
                    cfd_gen_add = (share * generation) - cfd_generation
                    cfd_generation += cfd_gen_add
                    cfd_revenue += (data['Capture'][i] + data['CfD_bonus'][i]) * cfd_gen_add
                    cfd_capture_revenue += (cfd_gen_add / data["Production"][i]) * data['Revenue'][i]
                    break  # stop initial loop as generation share fully reported in optimal way

            else:  # at a point with 0.85 <= cfd_generation / (share * generation) < 1 and not getting a bonus from CfD
                break  # stop initial loop as have fully optimised the period here

        merchant_generation = generation - cfd_generation
        merchant_revenue = capture_revenue - cfd_capture_revenue - penalty_cost
        total_revenue = cfd_revenue + merchant_revenue

        add_outputs = outputs_base.copy()
        add_outputs['Unoptimised_Revenue'] = (capture_revenue + unopt_cfd_bonus_rev)
        add_outputs['Unoptimised_Capture'] = add_outputs['Unoptimised_Revenue'] / generation
        add_outputs['Total_Revenue'] = total_revenue
        add_outputs['Effective_Capture'] = total_revenue / generation
        add_outputs['CfD_Generation'] = cfd_generation
        add_outputs['CfD_Revenue'] = cfd_revenue
        add_outputs['CfD_Effective_Capture'] = add_outputs['CfD_Revenue'] / add_outputs['CfD_Generation']
        add_outputs['Merchant_Generation'] = merchant_generation
        add_outputs['Merchant_Revenue'] = merchant_revenue
        add_outputs['Merchant_Effective_Capture'] = (
                add_outputs['Merchant_Revenue'] /
                add_outputs['Merchant_Generation'])
        add_outputs['Merchant_Penalty'] = penalty_cost
        add_outputs['Optimised_CfD_Discount'] = (
                add_outputs['CfD_Effective_Capture'] - run.strike_price)
        add_outputs['Merchant_Capture_Price_on_CfD_Days'] = (
                cfd_capture_revenue / add_outputs['CfD_Generation'])
        add_outputs['TGE_Base_on_CfD_Days'] = (
                add_outputs['Merchant_Capture_Price_on_CfD_Days'] +
                add_outputs['Optimised_CfD_Discount'])
        add_outputs['SRA_Deposit_per_MWh'] = (
                run.strike_price - add_outputs['TGE_Base_on_CfD_Days'])
        add_outputs['Total_SRA_Deposit'] = (
                add_outputs['SRA_Deposit_per_MWh'] *
                add_outputs['CfD_Generation'])

        if outputs is None:
            outputs = add_outputs
        else:
            outputs = pd.concat(
                [outputs, add_outputs], ignore_index=True
            )

    return outputs


def process_data(control, run, data):
    if control.use_DWH:
        data = dwh_process(run, data)
    else:
        data = data[["Timestamp", "Price", "LF_" + run.name]].rename(columns={"LF_" + run.name: "LF"})
    data = create_daily(control, run, data)
    data['Year'] = data['Date'].dt.year
    data['Month'] = data['Date'].dt.month
    if run.granularity == 'M':
        periods = sorted(set((('Year', date.year), ('Month', date.month)) for date in data['Date']))
    else:
        assert(run.granularity == 'Y')
        periods = sorted(set(('Year', date.year) for date in data['Date']))
    result = None
    for period in periods:
        period = dict(period)
        relevant_data = data.copy()
        for granularity in period:
            relevant_data = relevant_data[relevant_data[granularity] == period[granularity]]
        result_period = cfd_optimisation(relevant_data, control, run, period)
        if result is None:
            result = result_period
        else:
            result = pd.concat([result, result_period], ignore_index=True)
    return result
