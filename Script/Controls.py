import pandas as pd
import json
import datetime
from pathlib import Path


def read_json(filepath: str) -> dict:
    """
    Reads a json file.
    :param filepath: path to the json file
    """
    with open(Path(filepath), "r") as file:
        json_dict = json.load(file)
    return json_dict


class Run:

    name_counter = 0
    name_run_dict = {}

    def get_name(cls):
        suggest_name = f'run_{cls.name_counter}'
        cls.name_counter += 1
        if suggest_name in cls.name_run_dict.keys():
            return cls.get_name()
        return suggest_name

    def reset(cls):
        cls.name_counter = 0
        cls.name_run_dict = {}

    def __init__(self, **kwargs):

        self.name = kwargs.get('name', None)
        if self.name is None:
            self.name = self.get_name()

        self.tech = kwargs.get('technologies', ['sol'])
        if isinstance(self.tech, str):
            self.tech = [
                ''.join(letter for letter in tech if letter.isalpha())
                for tech in self.tech.split(',')
            ]
        self.tech = set(self.tech)

        self.subregions = kwargs.get('subregions', '')
        if pd.isnull(self.subregions):
            self.subregions = ''
        if len(self.subregions) <= 1 or self.subregions[0] == '-':
            self.subregions_method = 'black_list'
        else:
            self.subregions_method = 'white_list'
        self.subregions = [
            ''.join(letter for letter in subregion if letter.isalpha())
            for subregion in self.subregions.split(',')
        ]

        self.strike_price = float(kwargs.get('strike_price', 100))
        start_month = int(kwargs.get('start_month', 6))
        start_year = int(kwargs.get('start_year', 2023))
        duration = int(kwargs.get('duration_of_the_contract_in_months', 180))
        self.start_date = datetime.date(start_year, start_month, 1)
        self.end_date = datetime.date(
            start_year + (start_month + duration - 1) // 12,
            (start_month + duration - 1) % 12 + 1,
            1
        )

        self.rescale_LF = bool(int(kwargs.get('rescale_LF', False)))
        self.target_LF = kwargs.get('target_LF', None)
        self.penalty = bool(int(kwargs.get('penalty', True)))
        self.granularity = kwargs.get('granularity', 'M')
        self.capacity_to_MW = kwargs.get('capacity_to_MW', 1)

        self.name_run_dict[self.name] = self


class Control:
    def __init__(self, filepath):
        jdict = read_json(filepath)

        if not jdict['runs']:
            Run(**jdict['default_run'])
        else:
            runs = pd.read_csv(jdict['local_inputs']['runs'], index_col=0)
            run_names = runs.columns
            if 'all' not in jdict['runs']:
                run_names = [run_name for run_name in run_names if run_name in jdict['runs']]
            for run_name in run_names:
                Run(
                    name=run_name if 'Unnamed' not in run_name else None,
                    **{kwarg: runs.loc[kwarg, run_name] for kwarg in runs.index}
                )

        self.runs = Run.name_run_dict.copy()
        Run.reset(Run)
        self.start_date = min(run.start_date for run in self.runs.values())
        self.end_date = max(run.end_date for run in self.runs.values())
        self.DWH = jdict['DWH']
        self.DWH['technologies'] = set()
        for run in self.runs.values():
            self.DWH['technologies'] = self.DWH['technologies'].union(run.tech)
        self.use_DWH = jdict['use_inputs_from_DWH']
        self.scaling_method = jdict['scaling_method']
        self.local_inputs = jdict['local_inputs']
        self.outputs_folder = jdict['outputs_folder']

        shares_source = jdict['shares']['shares_source']
        if shares_source == 'file':
            self.shares = pd.read_csv(self.local_inputs['shares_file'], index_col=0, header=0)
        else:
            self.shares = pd.DataFrame(index=list(range(self.start_date.year, self.end_date.year + 1)))
            if shares_source == 'shares_generator':
                shares_generator = jdict['shares']['shares_generator']
                for share_value in range(
                        shares_generator['start_value'],
                        shares_generator['end_value'],
                        shares_generator['step']
                ):
                    self.shares[f'Share_{share_value}'] = share_value

