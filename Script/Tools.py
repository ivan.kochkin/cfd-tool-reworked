import numpy as np


def power_scale(nums, target, t_err=10 ** -4, ttl=100):
    power = 1
    counter = 0
    err = np.log(target) / np.log(nums.mean())
    temp_nums = nums
    while abs(err - 1) > t_err and counter < ttl:
        temp_nums = temp_nums ** err
        power *= err
        err = np.log(target) / np.log(temp_nums.mean())
        counter += 1
    return nums ** power


def lin_scale(nums, target, t_err=10 ** -4, ttl=100):
    if nums.mean() >= target:
        return nums * target / nums.mean()
    k = 1
    counter = 0
    while np.clip(k * nums, 0, 1).mean() < target - t_err and counter < ttl:
        k = k * target / np.clip(k * nums, 0, 1).mean()
        counter += 1
    return np.clip(k * nums, 0, 1)


scaling_by_method = {
    'power': power_scale,
    'linear': lin_scale
}