import os
import warnings
import re


def get_valid_filename(s):
    s = str(s).strip()
    s = re.sub(r"(?u)[^-\w.]", "_", s)
    s = re.compile(r"_{2,}").sub("_", s)

    return s


def make_directories(script_name):
    #  Function to make the directories for storing results

    #  Inputs:  script name -   the name of the script run

    direc = os.path.join("outputs")
    if not os.path.exists(direc):
        os.mkdir(direc)

    direc = os.path.join("outputs", script_name)
    if not os.path.exists(direc):
        os.mkdir(direc)
    else:
        warnings.warn(
            "error in making directories / script name as directory already exists but should not"
        )


def write_outputs(
        outputs,
        script_name,
        run,
        filename,
):

    outputs.to_csv(f'outputs/{script_name}/{filename}_{run.granularity}.csv', index=False)

    #  Now create yearly outputs for monthly optimisation granularity
    if run.granularity == 'M':

        y_outputs = (
            outputs
            .groupby(['Year'])
            .sum(min_count=1)
            .reset_index()
            .drop(["Month"], axis=1)
        )

        y_outputs["Capture_Price"] = y_outputs["Capture_Revenue"] / y_outputs["Generation"]

        y_outputs.to_csv(f'outputs/{script_name}/{filename}_Y.csv', index=False)
