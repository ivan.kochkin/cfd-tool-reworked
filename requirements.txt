boto3==1.26.69
keyring==23.13.1
numpy==1.24.2
pandas==1.5.3
psycopg2==2.9.5
