from Script.Controls import Control
from Script.DWH.DWH_processing import dwh_import
from Script.Optimization import process_data
import os
import pandas as pd

control = Control('./Inputs/Config/config.json')
if control.use_DWH:
    data = tuple(dwh_import(start_date=control.start_date, end_date=control.end_date, **control.DWH))
else:
    data = pd.read_csv(control.local_inputs['hourly'])

for run_name, run in control.runs.items():
    run_data = process_data(control, run, data)
    outputs_path = control.outputs_folder + f'{run_name}/'
    if not os.path.exists(outputs_path):
        os.makedirs(outputs_path)
    run_data.to_csv(outputs_path + 'results.csv', index=False)
